require 'sinatra'
require 'rack/oauth2'
require 'securerandom'

require_relative './lib'

enable :sessions

def get_client(profile_url)
  profile_config = get_profile_config profile_url

  Rack::OAuth2::Client.new(
    identifier: 'https://micropub-postman.herokuapp.com/',
    redirect_uri:  'https://micropub-postman.herokuapp.com/callback',
    host: 'micropub-postman.herokuapp.com',
    authorization_endpoint: profile_config[:authorization_endpoint],
    token_endpoint: profile_config[:token_endpoint],
  )
end

get '/' do
  erb :index
end

get '/start' do
  session[:profile_url] = params['profile_url']
  client = get_client session[:profile_url]

  session[:state] = SecureRandom.hex(32)

  authorization_uri = client.authorization_uri(
    scope: [:profile],
    state: session[:state]
  )

  redirect to(authorization_uri)
end

get '/callback' do
  client = get_client session[:profile_url]
  client.authorization_code = params['code']
  access_token = client.access_token! :body
  profile_config = get_profile_config access_token.raw_attributes['me']

  micropub_config = get_micropub_config(profile_config[:micropub], access_token)

  content_type :json
  build_collection(profile_config, micropub_config).to_json
end
