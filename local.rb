require_relative './lib'

profile_url = ARGV[0]
access_token = ARGV[1]
profile_config = get_profile_config(profile_url)

micropub_config = get_micropub_config(profile_config[:micropub], access_token)

jj build_collection(profile_config, micropub_config)
