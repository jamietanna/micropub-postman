require 'json'
require 'net/http'
require 'microformats'
require 'securerandom'

# via https://www.jvt.me/posts/2019/09/07/ruby-hash-keys-string-symbol/
class ::Hash
  # via https://stackoverflow.com/a/25835016/2257038
  def stringify_keys
    h = self.map do |k,v|
      v_str = if v.instance_of? Hash
                v.stringify_keys
              else
                v
              end

      [k.to_s, v_str]
    end
    Hash[h]
  end

  # via https://stackoverflow.com/a/25835016/2257038
  def symbol_keys
    h = self.map do |k,v|
      v_sym = if v.instance_of? Hash
                v.symbol_keys
              else
                v
              end

      [k.to_sym, v_sym]
    end
    Hash[h]
  end
end

def form_post_type(pt)
  body =
    {
      name: "#{pt[:name]} (Form)",
      request: {
        body: {
          mode: 'urlencoded',
          urlencoded: [
            {
            key: 'h',
            value: pt[:h],
            type: 'text',
            },
          ],
        },
        method: 'POST',
        description: "Publish a #{pt[:name]} (#{pt[:type]}) using the form-encoded format",
        headers: [],
        url: {
          host: %w({{micropub}}),
          query: [],
          raw: '{{micropub}}',
        },
      },
      "event": [
        {
          "listen": "test",
          "script": {
            "id": SecureRandom.uuid,
            "exec": [
              "pm.test(\"Post is created successfully\", () => {",
              "  pm.expect(pm.response.code).to.be.oneOf([201,202]);",
              "  pm.response.to.have.header(\"Location\");",
              "});"
            ],
            "type": "text/javascript"
          }
        }
      ],
  }

  pt[:properties].each do |p|
    body[:request][:body][:urlencoded] << {
      key: "#{p}[]",
      value: '',
      type: 'text',
      disabled: !pt[:"required-properties"].include?(p)
    }
  end

  body
end

def json_post_type(pt)
  json_body = {
    type: ["h-#{pt[:h]}"],
    properties: {},
  }

  pt[:properties].each do |p|
    json_body[:properties][p] = []
  end

  body =
    {
      name: "#{pt[:name]} (JSON)",
      request: {
        body: {
          mode: 'raw',
          raw: JSON.pretty_generate(json_body) +"\n",
          options: {
            raw: {
              language: 'json',
            }
          }
        },
        method: 'POST',
        description: "Publish a #{pt[:name]} (#{pt[:type]}) using the JSON format",
        headers: [],
        url: {
          host: %w({{micropub}}),
          query: [],
          raw: '{{micropub}}',
        },
      },
      "event": [
        {
          "listen": "test",
          "script": {
            "id": SecureRandom.uuid,
            "exec": [
              "pm.test(\"Post is created successfully\", () => {",
              "  pm.expect(pm.response.code).to.be.oneOf([201,202]);",
              "  pm.response.to.have.header(\"Location\");",
              "});"
            ],
            "type": "text/javascript"
          }
        }
      ],
  }
  body
end

def form_delete(is_delete)
  method = if is_delete
             'Delete'
           else
             'Undelete'
           end

  body =
    {
      name: "#{method} a post (Form)",
      request: {
        body: {
          mode: 'urlencoded',
          urlencoded: [
            {
              key: 'action',
              value: 'delete',
              type: 'text',
            },
            {
              key: 'url',
              value: '',
              type: 'text',
            },
          ],
        },
        method: 'POST',
        description: "#{method} a post, using a form request",
        headers: [],
        url: {
          host: %w({{micropub}}),
          query: [],
          raw: '{{micropub}}',
        },
      },
      "event": [
        {
          "listen": "test",
          "script": {
            "id": SecureRandom.uuid,
            "exec": [
              "pm.test(\"Post is created successfully\", () => {",
              "  pm.expect(pm.response.code).to.be.oneOf([201,202]);",
              "  pm.response.to.have.header(\"Location\");",
              "});"
            ],
            "type": "text/javascript"
          }
        }
      ],
  }

  body
end

def json_delete(is_delete)
  method = if is_delete
             'Delete'
           else
             'Undelete'
           end
  json_body = {
    action: 'delete',
    url: ''
  }

  {
    name: "#{method} a post (JSON)",
    request: {
      body: {
        mode: 'raw',
        raw: JSON.pretty_generate(json_body) +"\n",
        options: {
          raw: {
            language: 'json',
          }
        }
      },
      method: 'POST',
      description: "#{method} a post, using the JSON format",
      headers: [],
      url: {
        host: %w({{micropub}}),
        query: [],
        raw: '{{micropub}}',
      },
    },
    "event": [
      {
        "listen": "test",
        "script": {
          "id": SecureRandom.uuid,
          "exec": [
            "pm.test(\"Post is created successfully\", () => {",
            "  pm.expect(pm.response.code).to.be.oneOf([200,201,204]);",
            "});"
          ],
          "type": "text/javascript"
        }
      }
    ],
  }
end

def update
  json_body = {
    action: 'update',
    url: '',
  }

  {
    name: 'Update a post',
    request: {
      body: {
        mode: 'raw',
        raw: JSON.pretty_generate(json_body) +"\n",
        options: {
          raw: {
            language: 'json',
          }
        }
      },
      method: 'POST',
      description: "Update a post, using a JSON request",
      headers: [],
      url: {
        host: %w({{micropub}}),
        query: [],
        raw: '{{micropub}}',
      },
    },
    "event": [
      {
        "listen": "test",
        "script": {
          "id": SecureRandom.uuid,
          "exec": [
            "pm.test(\"Post is updated successfully\", () => {",
            "  pm.expect(pm.response.code).to.be.oneOf([200,201,202,204]);",
            "});"
          ],
          "type": "text/javascript"
        }
      }
    ],
  }
end

def q_config
  {
    name: "Query Configuration (q=config)",
    request: {
      method: 'GET',
      description: "Query the server's configuration",
      headers: [],
      url: {
        host: %w({{micropub}}),
        query: [
          {
            key: 'q',
            value: 'config',
          }
        ],
      },
    },
    "event": [
      {
        "listen": "test",
        "script": {
          "id": SecureRandom.uuid,
          "exec": [
            "pm.test(\"Query responds successfully\", () => {",
            "  pm.expect(pm.response.code).to.be.oneOf([200]);",
            "});"
          ],
          "type": "text/javascript"
        }
      }
    ],
  }
end

def q_syndication
  {
    name: "Query Syndication Targets (q=syndicate-to)",
    request: {
      method: 'GET',
      description: "Query the syndication targets for a given server",
      headers: [],
      url: {
        host: %w({{micropub}}),
        query: [
          {
            key: 'q',
            value: 'syndicate-to',
          }
        ],
      },
    },
    "event": [
      {
        "listen": "test",
        "script": {
          "id": SecureRandom.uuid,
          "exec": [
            "pm.test(\"Query responds successfully\", () => {",
            "  pm.expect(pm.response.code).to.be.oneOf([200]);",
            "});"
          ],
          "type": "text/javascript"
        }
      }
    ],
  }
end

def q_source
  {
    name: "Query Post Data (q=source)",
    request: {
      method: 'GET',
      description: "Query information about a given post's data",
      headers: [],
      url: {
        host: %w({{micropub}}),
        query: [
          {
            key: 'q',
            value: 'source',
          },
          {
            key: 'properties[]',
            value: '',
            disabled: true,
          },
          {
            key: 'url',
            value: '',
            disabled: true,
          },
        ],
      },
    },
    "event": [
      {
        "listen": "test",
        "script": {
          "id": SecureRandom.uuid,
          "exec": [
            "pm.test(\"Query responds successfully\", () => {",
            "  pm.expect(pm.response.code).to.be.oneOf([200]);",
            "});"
          ],
          "type": "text/javascript"
        }
      }
    ],
  }

end

def generic_query(q)
  body =
    {
      name: "q=#{q}",
      request: {
        method: 'GET',
        description: "Query the server for q=#{q}",
        headers: [],
        url: {
          host: %w({{micropub}}),
          query: [
            {
              key: 'q',
              value: q,
            }
          ],
        },
      },
      "event": [
        {
          "listen": "test",
          "script": {
            "id": SecureRandom.uuid,
            "exec": [
              "pm.test(\"Query responds successfully\", () => {",
              "  pm.expect(pm.response.code).to.be.oneOf([200]);",
              "});"
            ],
            "type": "text/javascript"
          }
        }
      ],
  }
  body
end

def default_post_types
  [
    {
      "h": "entry",
      "name": "Bookmark",
      "properties": [
        "bookmark-of",
        "name",
        "published",
        "category",
        "content",
        "syndication"
      ],
      "required-properties": [
        "bookmark-of",
        "name",
        "published"
      ],
      "type": "bookmark"
    },
    {
      "h": "entry",
      "name": "Like",
      "properties": [
        "like-of",
        "published",
        "category",
        "content",
        "name",
        "syndication"
      ],
      "required-properties": [
        "like-of",
        "published"
      ],
      "type": "like"
    },
    {
      "h": "entry",
      "name": "Reply",
      "properties": [
        "content",
        "in-reply-to",
        "published",
        "category",
        "name",
        "photo",
        "syndication",
        "mp-photo-alt"
      ],
      "required-properties": [
        "content",
        "in-reply-to",
        "published"
      ],
      "type": "reply"
    },
    {
      "h": "entry",
      "name": "Repost",
      "properties": [
        "published",
        "repost-of",
        "content",
        "category",
        "syndication"
      ],
      "required-properties": [
        "published",
        "repost-of"
      ],
      "type": "repost"
    },
    {
      "h": "entry",
      "name": "RSVP",
      "properties": [
        "in-reply-to",
        "published",
        "rsvp",
        "category",
        "content",
        "syndication"
      ],
      "required-properties": [
        "in-reply-to",
        "published",
        "rsvp"
      ],
      "type": "rsvp"
    },
    {
      "h": "entry",
      "name": "Note",
      "properties": [
        "content",
        "published",
        "category",
        "syndication"
      ],
      "required-properties": [
        "content",
        "published"
      ],
      "type": "note"
    },
  ]
end

def build_collection(profile_config, micropub_config)
  post_types = micropub_config[:'post-types']
  post_types = default_post_types if post_types.nil?

  form_items = post_types.collect { |pt| form_post_type(pt.symbol_keys) }
  json_items = post_types.collect { |pt| json_post_type(pt.symbol_keys) }
  queries = [q_config, q_syndication]
  unless micropub_config[:q].nil?
    micropub_config[:q].map {|q| generic_query(q) }.map {|q| queries << q }
  end

  {
    "info": {
      "_postman_id": SecureRandom.uuid,
      "name": "Micropub Configuration for #{profile_config[:profile_url]}",
      "schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
    },
    "item": [
      {
        name: "Query Server",
        item: queries,
      },
      {
        "name": "Create Post",
        "item": form_items.zip(json_items).flatten.compact,
      },
      {
        name: 'Read Post',
        item: [q_source]
      },
      {
        name: 'Update Post',
        item: [update]
      },
      {
        name: 'Delete Post',
        item: [form_delete(true), json_delete(true)]
      },
      {
        name: 'Undelete Post',
        item: [form_delete(false), json_delete(false)]
      },
    ],
    "auth": {
      "type": "oauth2",
      "oauth2": [
        {
          "key": "code_verifier",
          "value": "",
          "type": "string"
        },
        {
          "key": "addTokenTo",
          "value": "header",
          "type": "string"
        },
        {
          "key": "clientId",
          "value": "{{clientId}}",
          "type": "string"
        },
        {
          "key": "useBrowser",
          "value": true,
          "type": "boolean"
        },
        {
          "key": "accessTokenUrl",
          "value": "{{tokenEndpoint}}",
          "type": "string"
        },
        {
          "key": "authUrl",
          "value": "{{authorizationEndpoint}}",
          "type": "string"
        },
        {
          "key": "tokenName",
          "value": "IndieAuth token",
          "type": "string"
        },
        {
          "key": "scope",
          "value": "create read update delete",
          "type": "string"
        },
        {
          "key": "client_authentication",
          "value": "header",
          "type": "string"
        },
        {
          "key": "grant_type",
          "value": "authorization_code_with_pkce",
          "type": "string"
        }
      ]
    },
    "variable": [
      {
        "id": SecureRandom.uuid,
        "key": "micropub",
        "value": profile_config[:micropub],
      },
      {
        "id": SecureRandom.uuid,
        "key": "authorizationEndpoint",
        "value": profile_config[:authorization_endpoint],
      },
      {
        "id": SecureRandom.uuid,
        "key": "tokenEndpoint",
        "value": profile_config[:token_endpoint],
      },
      {
        "id": SecureRandom.uuid,
        "key": "clientId",
        "value": "https://www.postman.com/"
      },
    ],
  }
end

def get(url, access_token = nil)
  uri = URI.parse(url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true if uri.instance_of? URI::HTTPS
  request = Net::HTTP::Get.new(uri.request_uri)
  request['Authorization'] = "Bearer #{access_token}" unless access_token.nil?
  http.request(request)
end

def get_profile_config(profile_url)
  res = get(profile_url)

  parsed = Microformats.parse(res.body)
  {
    profile_url: profile_url,
    authorization_endpoint: parsed.rels['authorization_endpoint'][0],
    token_endpoint: parsed.rels['token_endpoint'][0],
    micropub: parsed.rels['micropub'][0],
  }
end

def get_micropub_config(micropub, access_token)
  res = get("#{micropub}?q=config", access_token)
  JSON.parse(res.body).symbol_keys
end
