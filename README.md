# Postman collection generator for Micropub

As detailed in [Autogenerating Postman Collections for Micropub](https://www.jvt.me/posts/2021/01/11/micropub-postman/), this is a Ruby tool to allow generating a Postman collection from a Micropub server.

## Usage

```sh
bundle config set path vendor/bundle
bundle install
# unauthenticated
bundle exec ruby local.rb https://www.staging.jvt.me
# with an access token
bundle exec ruby local.rb https://www.staging.jvt.me 'eyJ....'
```
